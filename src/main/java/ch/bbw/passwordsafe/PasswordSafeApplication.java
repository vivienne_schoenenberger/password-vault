package ch.bbw.passwordsafe;

import ch.bbw.passwordsafe.repository.ApplicationUserRepository;
import ch.bbw.passwordsafe.service.ApplicationUserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
@EnableWebSecurity
@SpringBootApplication
public class PasswordSafeApplication {

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	public static void main(String[] args) {
		SpringApplication.run(PasswordSafeApplication.class, args);
	}


}
