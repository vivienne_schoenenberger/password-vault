package ch.bbw.passwordsafe.repository;

import ch.bbw.passwordsafe.domain.ApplicationUser;
import ch.bbw.passwordsafe.domain.PasswordEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntryRepository extends JpaRepository<PasswordEntry, Long> {

    List<PasswordEntry> findAllByUser(ApplicationUser user);


}
