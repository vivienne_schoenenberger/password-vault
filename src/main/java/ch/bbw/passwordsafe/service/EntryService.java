package ch.bbw.passwordsafe.service;

import ch.bbw.passwordsafe.config.PasswordEncryptor;
import ch.bbw.passwordsafe.domain.ApplicationUser;
import ch.bbw.passwordsafe.domain.PasswordEntry;
import ch.bbw.passwordsafe.repository.EntryRepository;

import org.owasp.validator.html.*;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class EntryService {
    private EntryRepository entryRepository;
    String secret = "Shhhhhhhhht!!!";



    public EntryService(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    public PasswordEntry createEntry(PasswordEntry entry) throws ScanException, PolicyException {



        return entryRepository.saveAndFlush(entry);
    }

    public List<PasswordEntry> findAll() {
        return entryRepository.findAll();
    }
    public Optional<PasswordEntry> getEntryById(Long pw) {
        return entryRepository.findById(pw);
    }

    public void deleteById(Long pw){
        entryRepository.deleteById(pw);
    }
    public PasswordEntry save(PasswordEntry entry){

        entry.setPassword(PasswordEncryptor.encrypt(entry.getPassword(),secret));
        return entryRepository.save(entry);
    }

    public List <PasswordEntry> findByCurrentUser(ApplicationUser user){
        return entryRepository.findAllByUser(user);
    }



}
