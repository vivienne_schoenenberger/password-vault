package ch.bbw.passwordsafe.controller;

import ch.bbw.passwordsafe.config.JWTTokenHelper;
import ch.bbw.passwordsafe.config.PasswordEncryptor;

import ch.bbw.passwordsafe.domain.ApplicationUser;
import ch.bbw.passwordsafe.domain.PasswordEntry;
import ch.bbw.passwordsafe.repository.ApplicationUserRepository;
import ch.bbw.passwordsafe.service.EntryService;

import org.owasp.validator.html.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/entries")
public class EntryController {
    String secret = "shhhhhhhhhhhht!!";
    private final EntryService entryService;
    private PasswordEncryptor passwordEncryptor;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    JWTTokenHelper jWTTokenHelper;
    @Autowired
    private ApplicationUserRepository applicationUserRepository;


    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)


    public List<PasswordEntry> getAllEntriesByUser() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        ApplicationUser spezUser = applicationUserRepository.findByUsername(username);

        return entryService.findByCurrentUser(spezUser);

    }

    @GetMapping("/{id}")
    public PasswordEntry getEntryById(@PathVariable @NotNull Long id) {
        System.out.println("id" + id);
        return entryService.getEntryById(id).get();
    }

    @GetMapping("/pw/{id}")
    public String getDecryptedPasswordByID(@PathVariable @NotNull Long id) {
        System.out.println("id " + id);
         PasswordEntry foundEntry =entryService.getEntryById(id).get();
         String foundPassword = foundEntry.getPassword();
         return PasswordEncryptor.decrypt(foundPassword,secret);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PasswordEntry createUserEntry (@Valid @RequestBody PasswordEntry entry) throws ScanException, PolicyException {


        entry.setPassword(PasswordEncryptor.encrypt(entry.getPassword(),secret));
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        ApplicationUser spezUser = applicationUserRepository.findByUsername(username);
            entry.setUser(spezUser);

        //ANTISAMY here
        Policy policy = Policy.getInstance(Objects.requireNonNull(EntryController.class.getResourceAsStream("/antisamy-tinymce.xml")));
        AntiSamy antiSamy = new AntiSamy();
        System.out.println((antiSamy.scan(entry.getPassword(),policy)).getCleanHTML());

        entry.setPassword((antiSamy.scan(entry.getPassword(), policy)).getCleanHTML());
        entry.setNameTag((antiSamy.scan(entry.getNameTag(), policy)).getCleanHTML());
        entry.setRemarks((antiSamy.scan(entry.getRemarks(), policy)).getCleanHTML());
        entry.setUsage((antiSamy.scan(entry.getUsage(), policy)).getCleanHTML());


        return entryService.createEntry(entry);
    }


    @DeleteMapping( "/{id}")
    public void deletePassword(@PathVariable Long id){
        entryService.deleteById(id);
    }

    @PutMapping("/{id}")
    public PasswordEntry updateEntry (@PathVariable Long id, @RequestBody PasswordEntry newEntry){

        return entryService.getEntryById(id)
                .map(entry -> {
                    entry.setPassword(newEntry.getPassword());
                    entry.setRemarks(newEntry.getRemarks());
                    entry.setUsage(newEntry.getUsage());
                    entry.setNameTag(newEntry.getNameTag());

                    return entryService.save(entry);
                })
                .orElseGet(() -> {
                    newEntry.setId(id);
                    return entryService.save(newEntry);
                });

    }

}
