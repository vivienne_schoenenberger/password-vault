package ch.bbw.passwordsafe.controller;

import akka.http.javadsl.model.HttpHeader;
import ch.bbw.passwordsafe.config.JWTTokenHelper;
import ch.bbw.passwordsafe.domain.ApplicationUser;
import ch.bbw.passwordsafe.repository.ApplicationUserRepository;
import ch.bbw.passwordsafe.request.AuthCredentialsRequest;
import ch.bbw.passwordsafe.responses.LoginResponse;
import com.auth0.jwt.JWT;
import io.github.nefilim.kjwt.JWTSignError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;

import static ch.bbw.passwordsafe.config.SecurityConstants.*;
import static ch.bbw.passwordsafe.config.SecurityConstants.TOKEN_PREFIX;
import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
@RestController
@RequestMapping("/users")
public class UserController {

    private final ApplicationUserRepository applicationUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    JWTTokenHelper jWTTokenHelper;

    public UserController(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    @GetMapping("/username")
    public String currentUserName(Authentication authentication) {
        return authentication.getName();
    }
    @PostMapping("/signUp")
    @CrossOrigin(origins = "http://localhost:3000")
    public void signUp(@RequestBody ApplicationUser user) {
        //hier wird das userpassword encoded:
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
        System.out.println("at signup");
    }
    @PostMapping("/login")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<?> login(@RequestBody AuthCredentialsRequest authCredentialsRequest) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException {
        final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authCredentialsRequest.getUsername(), authCredentialsRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        User user = (User) authentication.getPrincipal();

        String jwtToken = jWTTokenHelper.generateToken(user.getUsername());

        LoginResponse response = new LoginResponse();
        response.setToken(jwtToken);

        return ResponseEntity.ok(response);


    }

}
